<?php
header('Content-Type: text/html; charset=UTF-8');

//$name = $_POST['name'];
// $email = $_POST['email'];
// $birthdate = $_POST['birthdate'];
// $sex = $_POST['radio1'];
// $limbs = $_POST['radio2'];
// $superpower = $_POST['spselector'];
// $biography = $_POST['about'];
// $agreement = $_POST['agreement'];
// // Параметры для подключения
// $db_host = "localhost"; 
// $db_user = "u40171"; // Логин БД
// $db_password = "4563456"; // Пароль БД
// $db_base = 'u40171'; // Имя БД
// $db_table = 'application'; // Имя Таблицы БД


if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Массив для временного хранения сообщений пользователю.
    $messages = array();

    // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
    // Выдаем сообщение об успешном сохранении.
    if (!empty($_COOKIE['save'])) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('save', '', 100000);
      // Если есть параметр save, то выводим сообщение пользователю.
      $messages[] = 'Спасибо, результаты сохранены.';
      $result = false;
      try {
            // Подключение к базе данных
            $db = new PDO('mysql:host=localhost;dbname=u40171', 'u40171', '4563456');
            // Устанавливаем корректную кодировку
            $db->exec("set names utf8");
            $data = array('name' => empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value']); 

            $query = $db->prepare("INSERT INTO application (name) values (:name)");

            // Выполняем запрос с данными
            $query->execute($data);
            $result = true;
        
        } catch (PDOException $e) {
            // Если есть ошибка соединения, выводим её
            print "Ошибка!: " . $e->getMessage() . "<br/>";
        }
        

    if ($result) {print "Успех!";}
    }

    // Складываем признак ошибок в массив.
    $errors = array();
    $errors['name'] = !empty($_COOKIE['name_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['date'] = !empty($_COOKIE['date_error']);
    $errors['agreement'] = !empty($_COOKIE['agreement_error']);
    // TODO: аналогично все поля.

    // Выдаем сообщения об ошибках.
    if ($errors['name']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('name_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Заполните имя.</div>';

    }
    //email
    if ($errors['email']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('email_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Строка email не заполнена либо содержит недопустимые символы, например: ! ; $ # % ^ и т.д.</div>';

    }
    //date
    if ($errors['date']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('date_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Заполните дату.</div>';

    }
    //agreement
    if ($errors['agreement']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('agreement_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Примите соглашение.</div>';

    }
    // TODO: тут выдать сообщения об ошибках в других полях.

    // Складываем предыдущие значения полей в массив, если есть.
    $values = array();
    $values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['date'] = empty($_COOKIE['date_value']) ? '' : $_COOKIE['date_value'];
    $values['agreement'] = empty($_COOKIE['agreement_value']) ? '' : $_COOKIE['agreement_value'];
    // TODO: аналогично все поля.

    // Включаем содержимое файла form.php.
    // В нем будут доступны переменные $messages, $errors и $values для вывода 
    // сообщений, полей с ранее заполненными данными и признаками ошибок.
    include('test.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
      // Проверяем ошибки.
      $errors = FALSE;
      if (empty($_POST['name'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('name_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
      }
      else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60 * 12);
      }
      //email
      if (empty($_POST['email']) or preg_match("A-Za-z0-9@-_.", $_POST['email'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
      }
      else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60 * 12);
      }
      //date
      if (empty($_POST['date'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('date_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
      }
      else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('date_value', $_POST['date'], time() + 30 * 24 * 60 * 60 * 12);
      }
      //agreement
      if (empty($_POST['agreement'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('agreement_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
      }
      else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('agreement_value', $_POST['agreement'], time() + 30 * 24 * 60 * 60 * 12);
      }
    // *************
    // TODO: тут необходимо проверить правильность заполнения всех остальных полей.
    // Сохранить в Cookie признаки ошибок и значения полей.
    // *************

      if ($errors) {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        header('Location: action.php');
        exit();
      }
      else {
        // Удаляем Cookies с признаками ошибок.
        setcookie('name_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('date_error', '', 100000);
        setcookie('agreement_error', '', 100000);
      }

      // Сохранение в XML-документ.
      // ...

      // Сохраняем куку с признаком успешного сохранения.
      setcookie('save', '1');

      // Делаем перенаправление.
      header('Location: action.php');
}
    

// try {
//     // Подключение к базе данных
//     $db = new PDO('mysql:host=localhost;dbname=u40171', $db_user, $db_password);
//     // Устанавливаем корректную кодировку
//     $db->exec("set names utf8");
//     $data = array('name' => $name, 'email' => $email, 'birthdate' => $birthdate, 'sex' => $sex, 'limbs' => $limbs, 'superpower' => $superpower,'biography' => $biography, 'agreement' => $agreement); 

//     $query = $db->prepare("INSERT INTO application (name, eemail, ddate, sex, limbs
//     superpower, description, consent) values (:name, :email, :birthdate, :sex, :limbs, :superpower,
//     :biography, :agreement)");

//  // Выполняем запрос с данными
//  $query->execute($data);
//  $result = true;
    
// } catch (PDOException $e) {
//     // Если есть ошибка соединения, выводим её
//     print "Ошибка!: " . $e->getMessage() . "<br/>";
// }

?>